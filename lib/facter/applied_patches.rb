Facter.add('applied_patches') do
  setcode do
    
    case Facter.value(:osfamily)
      when 'windows'
        # powershell='C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe'
        # output = Facter::Core::Execution.execute("#{powershell} -command 'Get-ChildItem -Path C:\patches | Format-Table name -HideTableHeaders'")
        output = Dir["C:/patches/**/*"]
        output.map { |patch| File.basename(patch) }
      else
        output = Facter::Core::Execution.execute('ls /patches | grep ^patch-')
        output.lines.map(&:chomp)
    end

  end
end
