Facter.add('patch_count') do
  setcode do
    case Facter.value(:osfamily)
    when 'windows'
      # powershell='C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe'
      # Facter::Core::Execution.execute("#{powershell} -command 'Get-ChildItem -Path C:\patches | Format-Table name -HideTableHeaders'")
      Dir["C:/patches/**/*"].length
    else
      Facter::Core::Execution.execute('ls -l /patches | grep ^- | wc -l')
    end
  end
end
