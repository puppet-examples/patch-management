# @summary Patch Management Example
#
# @example
#   include patch_management
class patch_management (
  String $patch_dir
) {
  file { 'patch_dir':
    ensure => 'directory',
    path   => $patch_management::patch_dir,
  }
}
